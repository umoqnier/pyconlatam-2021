# Esquites con limón y chile (un framework de software libre para corpus paralelos)

Presentación para el [PyCon Latam 2021](https://www.pylatam.org/) (si me la aceptan).

## Propuesta

- __Tema:__ Un proyecto interesante que tengo
- __Duración__: 35 min.

## Abstracto

[Esquite](https://github.com/ElotlMX/Esquite/) es un _framework_ de software
libre que gestiona y permite consultar corpus paralelos (textos bilingües)
mediante un sistema web y una API. Primordialmente facilita la presencia de
datos lingüísticos escasos en Internet.

El **software libre** como filosofia y forma de desarrollo de software permite la
creación de proyectos colaborativos, descentralizados y abiertos que benefician
a toda una comunidad.

## Descripción

El software libre como filosofía y forma de desarrollo de software permite la
creación de proyectos colaborativos, descentralizados y abiertos que benefician
a toda una comunidad.

Esquite es un _framework_ de software libre destinado a
personas que poseen corpus paralelos (textos bilingües) y que desean tener un
sistema web que les permita administrar y realizar búsquedas a través
de sus corpus.

Además, Esquite integra una API para que desarrolladoræs consulten la
información del corpus de forma programática facilitando el desarrollo de
aplicaciones de terceros.

Este tipo de desarrollos es importante, ya que facilita que personas pongan en
línea datos lingüísticos que son actualmente escasos, por ejemplo,
textos en lenguas indígenas o de bajos recursos digitales.

En particular, en México se concentran 68 agrupaciones lingüísticas (con sus más
de 300 variantes) y a pesar de que poseen el carácter de “lengua nacional”,
difícilmente los portales gubernamentales, educativos, culturales, oficiales,
etc. ofrecen material en estas lenguas.

Por un lado, Esquite puede contribuir a aumentar la disponibilidad de contenido
digital en lenguas que históricamente han sido minorizadas. Por otro lado, estos
recursos son muy valiosos para desarrollar tecnologías del lenguaje útiles para
hablantes, estudiosos de las lenguas, programadoæs entre otræs. Algunos ejemplos
son:

* Traductores automáticos
* Consulta de corpus en línea
    * [Tsunkua](https://tsunkua.elotl.mx/)
    * [Kolo](https://kolo.elotl.mx/)
* APIs
    * [Elotl](https://api.elotl.mx/)
* Herramientas para _Natural Language Processing (NLP)_

Esquite es impulsado por Comunidad Elotl, una organización
multidisciplinaria sin fines de lucro enfocada en el desarrollo de tecnologías
para lenguas mexicanas. En esta charla les presentaré como utilizamos `python`,
`django` y `elasticsearch` para del desarrollo del *framework* Esquite y un
flujo sugerido de contribución en caso de que deseen ampliar su desarrollo.

Este material se ha presentado en el **CCOSS 2020** -> [Video](https://ccoss.org/sessions/esquite/)

### Plan de acción

* ¿Quién soy? - 2 min
* Software Libre - 6 min
  * Libertades
	* Comunidades
* Diversidad lingüística en México - 12 min
	* Comunidad Elotl
* Esquite - 15 min
  * ¿Qué es?
  * ¿Qué hace?
	* ¿Por qué lo hace?
  * ¿Cómo contribuir?
* Preguntas - 5 min

## Notas

* Requerimientos: Posibilidad de presentar mi pantalla en vivo
* Desde hace un año investigo temas relacionados con lenguas mexicanas y NLP.
 Soy parte de [Comunidad Elotl](https://elotl.mx/) y maintainer del framework
 Esquite. Tambien colaboro con la comunidad de desarrollo de software libre
 [LIDSoL](https://lidsol.org/)

